from jinja2 import Template
import json
import os


def make_readme():
    config = json.load(open(".openpatch.json"))
    template = Template(open("README.md.j2").read())

    content = []
    for path in config.get("content", []):
        content.append(open(path).read())

    with open("README.md", "w") as readme:
        readme.write(template.render(config, content=content))


def init():
    default_config = {
        "name": os.getenv("CI_PROJECT_TITLE", "Project Name"),
        "description": os.getenv("CI_PROJECT_URL", "Project Description"),
        "badges": [],
        "contributors": [
            {
                "name": os.getenv("GITLAB_USER_NAME", "Name"),
                "email": os.getenv("GITLAB_USER_EMAIL", "E-Mail"),
                "url": f"https://gitlab.com/{os.getenv('GITLAB_USER_LOGIN')}",
                "img": f"https://assets.gitlab-static.net/uploads/-/system/user/avatar/{os.getenv('GITLAB_USER_ID')}/avatar.png",
            }
        ],
        "license": "MIT",
        "content": [],
    }

    with open(".openpatch.json", "w") as config:
        json.dump(default_config, config, indent=2)


if __name__ == "__main__":
    if os.path.exists(".openpatch.json"):
        make_readme()
    else:
        init()
        make_readme()
