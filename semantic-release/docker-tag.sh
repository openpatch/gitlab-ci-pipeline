#!/bin/sh
docker pull $CI_REGISTRY_IMAGE:latest
docker tag $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$1
docker push $CI_REGISTRY_IMAGE:$1

docker login --username $DOCKER_USER --password $DOCKER_TOKEN

docker tag $CI_REGISTRY_IMAGE:$1 $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$1
docker push $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$1
