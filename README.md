# Usage

1. Protect the master branch, otherwise the GITLAB_TOKEN will not be available
2. Create a .gitlab.yml file in your repository with the following content:

```
stages:
  - build
  - docs
  - tag
  - release

# set environment variables
variables:
  RABBITMQ_HOST: "mock"

include:
  - project: 'openpatch/gitlab-ci-pipeline'
    file: 'build-and-release.yml'
```

or for a project based on the flask-microservice template:

```
stages:
  - build
  - test
  - docs
  - tag
  - release

# set environment variables
variables:
  RABBITMQ_HOST: "mock"

include:
  - project: 'openpatch/gitlab-ci-pipeline'
    file: 'flask-microservice.yml'
```

3. Follow our semantic-release guidelines for automatic releases: https://gitlab.com/openpatch/semantic-release

## build-and-release

It builds a docker container and creates a release based on semantic release.

## flask-microservice

It includes build-and-release, tests a flask application for coverage and
creates an ER-diagram based on the code.

## readme

It updates a readme based on a .openpatch.json. An example:

```json
{
  "name": "Semantic Release Test",
  "description": "This **is** a description",
  "logo": "https://assets.gitlab-static.net/uploads/-/system/group/avatar/3174248/logo_clean.png",
  "badges": [
    {
      "alt": "Custom badge",
      "url": "",
      "img": "https://gitlab.com/openpatch/semantic-release-test/badges/master/pipeline.svg"
    }
  ],
  "contributors": [
    {
      "name": "Mike Barkmin",
      "email": "mike@openpatch.org",
      "url": "https://twitter.com/mikebarkmin",
      "img": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/1307206/avatar.png"
    }
  ],
  "license": "MIT",
  "content": [".gitlab/01_introduction.md"]
}
```
